// #### Технические требования:
// - Написать функцию `createNewUser()`, которая будет создавать и возвращать объект `newUser`.
// - При вызове функция должна спросить у вызывающего имя и фамилию.
// - Используя данные, введенные пользователем, создать объект `newUser` со свойствами `firstName` и `lastName`.
// - Добавить в объект `newUser` метод `getLogin()`, который будет возвращать первую букву имени пользователя, соединенную с фамилией пользователя, все в нижнем регистре (например, `Ivan Kravchenko → ikravchenko`).
// - Создать пользователя с помощью функции `createNewUser()`. Вызвать у пользователя функцию `getLogin()`. Вывести в консоль результат выполнения функции.

// #### Необязательное задание продвинутой сложности:
// - Сделать так, чтобы свойства `firstName` и `lastName` нельзя было изменять напрямую. Создать функции-сеттеры `setFirstName()` и `setLastName()`, которые позволят изменить данные свойства.


function createNewUser() {
    let firstName = prompt('Enter your name')
    while(!firstName) {
        firstName = prompt('Enter your real name!')
    }
    let lastName = prompt('Enter your last name')
    while(!lastName) {
        lastName = prompt('Enter your real last name!')
    }
        const newUser = {
        firstName,
        lastName,
        getLogin: function() {
            return this.firstName[0].toLowerCase()+this.lastName.toLowerCase()          
        }
    }
    return newUser.getLogin()
}

console.log(createNewUser())
